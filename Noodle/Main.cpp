#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
using namespace sf;

const int SCRWIDTH = 400; // Largeur de l'�cran
const int SCRHEIGHT = 533; // Hauteur de l'�cran
struct point
{
    int x, y; // variable erticale et horizontale 

};

int main()
{
    srand(time(0));

    RenderWindow app(VideoMode(SCRWIDTH, SCRHEIGHT), "Noodle Game!", Style::Close);
    app.setFramerateLimit(60);

    Texture t1, t2, t3, t4;
    t1.loadFromFile("images/konoha.jpg"); // Chargement de l'image du background
    t2.loadFromFile("images/platform2.png"); // Chargement de l'image des plateformes
    t3.loadFromFile("images/doodle.png");  // Chargement de l'image du personnage
    t4.loadFromFile("images/looser2.jpg");// Chargement de l'image de game over
    Music music,music2;
    if (!music.openFromFile("sound/ost.ogg")) {
        return -1; // erreur si le fichier n'est pas trouv� ou ne peut �tre ouvert
    }
    music.play(); // Lancement de la musique
   
    music2.openFromFile("sound/lose.ogg");


    Sprite sBackground(t1), sPlat(t2), sPers(t3),sLoose(t4);
    sPlat.setScale(1.5, 1);
    sLoose.setScale(1.6, 1.8);
    point plat[20];

    for (int i = 0;i < 10;i++)
    {
        plat[i].x = rand() % 400;
        plat[i].y = rand() % 533;
    }

    int x = 100, y = 50, h = 200;
    float dx = 0, dy = 0;

    while (app.isOpen())
    {
        Event e;
        while (app.pollEvent(e))
        {
            if (e.type == Event::Closed) 
            {
                app.close();
            }
               
        }

        if (Keyboard::isKeyPressed(Keyboard::Right)) 
        {
            x += 3;
        }
           
        if (Keyboard::isKeyPressed(Keyboard::Left))
        {
            x -= 3;
        }
        dy += 0.2;
        y += dy;
        if (y > 500) 
        {
            
            app.clear(); 
            music.stop();
            music2.play();
            music2.setLoop(true); // Musique en boucle
            // Texte du game Over
            Font font;
            font.loadFromFile("font/Manga.otf");
            Text text("Game Over", font);
            text.setCharacterSize(80);
            text.setStyle(Text::Bold);
            text.setFillColor(Color(255, 135, 31));


            FloatRect textRect = text.getLocalBounds();
            text.setOrigin(textRect.left + textRect.width / 2.0f,
            textRect.top + textRect.height / 2.0f);
            text.setPosition(Vector2f(SCRWIDTH / 2.0f, SCRHEIGHT / 2.0f));


            app.draw(sLoose);
            app.draw(text);
          // Initialisation de l'image de game over
            app.display();
            //dy = -10;  saut du perso dans le vide


            while (!(Keyboard::isKeyPressed(Keyboard::Space)))
            {
                while (app.pollEvent(e))
                {
                    if (e.type == Event::Closed)
                    {
                        app.close();
                    }

                }
                y = 50;
                dy = -15;

            }
            music2.stop();
            music.play(); // Lancement de la musique
            music.setLoop(true); // Musique en boucle
        }
        else {
            if (y < h)
                for (int i = 0;i < 10;i++)
                {
                    y = h;
                    plat[i].y = plat[i].y - dy;
                    if (plat[i].y > 533)
                    {
                        plat[i].y = 0;
                        plat[i].x = rand() % 400;
                    }
                }

            for (int i = 0;i < 10;i++)
                if ((x + 50 > plat[i].x) && (x + 20 < plat[i].x + 68) && (y + 70 > plat[i].y) && (y + 70 < plat[i].y + 14) && (dy > 0))
                {
                    dy = -12; // saut du perso sur une plateforme
                }



            sPers.setPosition(x, y);

            app.draw(sBackground); // initialisation du background
            app.draw(sPers);// initialisation du personnage
            for (int i = 0;i < 10;i++)
            {
                sPlat.setPosition(plat[i].x, plat[i].y);
                app.draw(sPlat);
            }

            app.display();
        }

    }

    return 0; // fin de l'application
}
